## Proyecto Base Servidor Web (Spring-PostgreSQL)
### Requisitos
1. Java 1.8
2. Gradle 2.12 o superior
3. Plugin EditorConfig
4. Project Lombok

### Recomendados
1. Eclipse, Spring Tool Suite, NetBeans o IntelliJ IDEA.
2. Plugin Buildship Gradle Integration

### Instalación
Eclipse o Spring Tool Suite

Para poder generar el proyecto se debe ejecutar los siguientes comandos:

Ubicarse en la raíz del proyecto.
```
cd server/
```
Para generar las configuraciones de IDE Eclipse.
```
gradle eclipse
```

Una vez generadas las configuraciones se procede a Importar el proyecto mediante Eclipse.



### Configuración

La configuraciones básicas del proyecto se encuentra en el archivo properties de la carpeta resources.

```
server/src/main/resources/
```

Una vez localizado el archivo debe configurar el usuario, nombre y contraseña de su motor de base de datos.
```
spring.datasource.username=postgres
spring.datasource.url=jdbc:postgresql://localhost:5432/nombreBDD
spring.datasource.password=xxxxxxxxxxxxxxxxxx
```

En caso de utilizar las funciones de envío de emails, cambie las configuraciones acorde a sus necesidades.
```
spring.mail.host = smtp.gmail.com
spring.mail.username = username@gmail.com
spring.mail.password = xxxxxxxxxxxxxxxxxx
spring.mail.port=587
```

### Migración de Base de Datos

Antes de ejecutar el proyecto proceda a realizar las respectivas migraciones iniciales de la base de datos.

Las migraciones están ubicadas en el directorio changelog ubicado dentro de la carpeta db.
```
server/src/main/resources/db/changelog
```
En el directorio versions se agregan la estructura de la base de datos.
```
server/src/main/resources/db/changelog/versions
```
En el directorio data se agregan los registros a cargar (Datos de pruebas).
```
server/src/main/resources/db/changelog/data
```
En el directorio sql se agregan las sentencias SQL las que no pueden ser creados con xml o yaml por diferir entre motores de base de datos. (Trigger o stored procedure).
```
server/src/main/resources/db/changelog/sql
```

### Ejecución del Proyecto

Para ejecutar el proyecto dentro del IDE.

```
Click derecho en el proyecto, Run As -> Spring Boot App
```

Si desea ejecutar el proyecto por medio de la consola.
```
gradle bootRun
```

### Ayuda
Para mayor ayuda referirse a la documentación

1. [Gradle] (https://gradle.org/) 
2. [Spring] (https://projects.spring.io/spring-boot/)
3. [STS] (https://spring.io/tools/eclipse)
4. [Liquibase] (http://www.liquibase.org/)
5. [Project Lombok] (https://projectlombok.org/)

