#!/usr/bin/bash
generarEstructura=false
generarDatos=false
generateOption=1

host=localhost
port=5432
bddName=ERPServer
user=postgres
pass=12123
schema=public
tipeGenerate=yaml
driverPath=postgresql-9.4.1207.jar

if [ $generateOption -eq 1 ]
    then
    generarEstructura=true
fi
if [ $generateOption -eq 2 ]
    then
    generarEstructura=true
fi
if [ $generateOption -eq 3 ]
    then
    generarEstructura=false
    generarDatos=false
fi
if $generarEstructura;  then
	echo "Generando estructura.."

  ./liquibase --driver=org.postgresql.Driver \
      --classpath=$driverPath \
      --changeLogFile=generated/db.changelog.$tipeGenerate \
      --url="jdbc:postgresql://"$host":"$port"/"$bddName \
      --username=$user \
      --password=$pass \
      --defaultSchemaName=$schema \
      generateChangeLog
fi

if $generarEstructura;  then
   echo "Generando datos.."

  ./liquibase --driver=org.postgresql.Driver \
      --classpath=$driverPath \
      --changeLogFile=generated/db.changelog.$tipeGenerate \
      --url="jdbc:postgresql://"$host":"$port"/"$bddName \
      --username=$user \
      --password=$pass \
      --defaultSchemaName=$schema \
      --diffTypes="data" \
      generateChangeLog
fi
echo "v";