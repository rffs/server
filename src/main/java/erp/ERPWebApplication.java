package erp;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
(exclude = {
    org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class})
@EntityScan(basePackages = { "*.modulos.*.model","model" })
@EnableJpaRepositories(basePackages = "*.modulos.*.repository")
@ComponentScan(basePackages = { "*.modulos.*.controller", "*.utilities" })
@EnableTransactionManagement
public class ERPWebApplication 
//extends WebSecurityConfigurerAdapter 
{

//  @Autowired
//  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//    auth.inMemoryAuthentication().withUser("user").password("user").roles("USER");
//  }
//
//  @Override
//  protected void configure(HttpSecurity http) throws Exception {
//    http.authorizeRequests().anyRequest().authenticated().and().httpBasic().and().csrf().disable();
//  }

  public static void main(String[] args) {
    SpringApplication.run(ERPWebApplication.class, args);
  }
}
