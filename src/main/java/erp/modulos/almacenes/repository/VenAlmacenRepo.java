package erp.modulos.almacenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.VenAlmacene;


@Repository
public interface VenAlmacenRepo extends JpaRepository<VenAlmacene, String> {

}
