package erp.modulos.clientes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import erp.modulos.almacenes.repository.VenAlmacenRepo;
import erp.modulos.clientes.repository.VenClienteRepo;
import model.VenAlmacene;
import model.VenCliente;

@RestController
@RequestMapping(value = "/cliente")
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
public class VenClienteRESTController {

	@Autowired
	private VenClienteRepo venClienteRepo;

	@Autowired
	private VenAlmacenRepo venAlmacenRepo;

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<VenCliente> getTodos() {
		return this.venClienteRepo.getByEstado("a");
	}

	@RequestMapping(value = "/{dni}", method = RequestMethod.GET)
	public @ResponseBody List<VenCliente> getClienteByDNI(@PathVariable(value = "dni") String dni) {
		return this.venClienteRepo.findByDNI(dni);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody VenCliente addCliente(@RequestBody VenCliente cliente) {
		System.out.println(cliente);
		this.venClienteRepo.save(cliente);
		return cliente;
	}

	@RequestMapping(value = "/almacen", method = RequestMethod.GET)
	public @ResponseBody List<VenAlmacene> getTodosAlmacen() {
		return this.venAlmacenRepo.findAll();
	}
}
