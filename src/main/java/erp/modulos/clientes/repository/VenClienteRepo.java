package erp.modulos.clientes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import model.VenCliente;

@Repository
// @CrossOrigin(origins = "http://localhost:4200")
public interface VenClienteRepo extends JpaRepository<VenCliente, String> {

  @Query("SELECT i FROM VenCliente i WHERE i.cliDni = :codId")
  public List<VenCliente> findByDNI(@Param("codId") String codId);
  
  @Query("SELECT i FROM VenCliente i WHERE i.cliEstado = :cliEstado")
  public List<VenCliente> getByEstado(@Param("cliEstado") String cliEstado);
}
