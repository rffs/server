package erp.modulos.almacencaja.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import model.VenAlmacenescaja;

@Repository
public interface VenAlmacenescajaRepo extends CrudRepository<VenAlmacenescaja, String> {

}
