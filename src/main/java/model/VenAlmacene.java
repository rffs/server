package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ven_almacenes database table.
 * 
 */
@Entity
@Table(name="ven_almacenes")
@NamedQuery(name="VenAlmacene.findAll", query="SELECT v FROM VenAlmacene v")
public class VenAlmacene implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="alm_id")
	private String almId;

	@Column(name="alm_estado")
	private String almEstado;

	@Column(name="alm_nombre")
	private String almNombre;

	//bi-directional many-to-one association to VenAlmacenescaja
	@OneToMany(mappedBy="venAlmacene")
	private List<VenAlmacenescaja> venAlmacenescajas;

	public VenAlmacene() {
	}

	public String getAlmId() {
		return this.almId;
	}

	public void setAlmId(String almId) {
		this.almId = almId;
	}

	public String getAlmEstado() {
		return this.almEstado;
	}

	public void setAlmEstado(String almEstado) {
		this.almEstado = almEstado;
	}

	public String getAlmNombre() {
		return this.almNombre;
	}

	public void setAlmNombre(String almNombre) {
		this.almNombre = almNombre;
	}

	public List<VenAlmacenescaja> getVenAlmacenescajas() {
		return this.venAlmacenescajas;
	}

	public void setVenAlmacenescajas(List<VenAlmacenescaja> venAlmacenescajas) {
		this.venAlmacenescajas = venAlmacenescajas;
	}

	public VenAlmacenescaja addVenAlmacenescaja(VenAlmacenescaja venAlmacenescaja) {
		getVenAlmacenescajas().add(venAlmacenescaja);
		venAlmacenescaja.setVenAlmacene(this);

		return venAlmacenescaja;
	}

	public VenAlmacenescaja removeVenAlmacenescaja(VenAlmacenescaja venAlmacenescaja) {
		getVenAlmacenescajas().remove(venAlmacenescaja);
		venAlmacenescaja.setVenAlmacene(null);

		return venAlmacenescaja;
	}

}