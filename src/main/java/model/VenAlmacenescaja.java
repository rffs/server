package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ven_almacenescajas database table.
 * 
 */
@Entity
@Table(name="ven_almacenescajas")
@NamedQuery(name="VenAlmacenescaja.findAll", query="SELECT v FROM VenAlmacenescaja v")
public class VenAlmacenescaja implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="caj_id")
	private String cajId;

	@Column(name="alm_numeracion")
	private String almNumeracion;

	@Column(name="caj_estado")
	private String cajEstado;

	@Column(name="caj_nombre")
	private String cajNombre;

	//bi-directional many-to-one association to VenAlmacene
	@ManyToOne
	@JoinColumn(name="alm_id")
	private VenAlmacene venAlmacene;

	public VenAlmacenescaja() {
	}

	public String getCajId() {
		return this.cajId;
	}

	public void setCajId(String cajId) {
		this.cajId = cajId;
	}

	public String getAlmNumeracion() {
		return this.almNumeracion;
	}

	public void setAlmNumeracion(String almNumeracion) {
		this.almNumeracion = almNumeracion;
	}

	public String getCajEstado() {
		return this.cajEstado;
	}

	public void setCajEstado(String cajEstado) {
		this.cajEstado = cajEstado;
	}

	public String getCajNombre() {
		return this.cajNombre;
	}

	public void setCajNombre(String cajNombre) {
		this.cajNombre = cajNombre;
	}

	public VenAlmacene getVenAlmacene() {
		return this.venAlmacene;
	}

	public void setVenAlmacene(VenAlmacene venAlmacene) {
		this.venAlmacene = venAlmacene;
	}

}