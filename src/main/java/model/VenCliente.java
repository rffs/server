package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ven_clientes database table.
 * 
 */
@Entity
@Table(name="ven_clientes")
@NamedQuery(name="VenCliente.findAll", query="SELECT v FROM VenCliente v")
public class VenCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cli_dni")
	private String cliDni;

	@Column(name="cli_celular")
	private String cliCelular;

	@Column(name="cli_contabilidad")
	private Boolean cliContabilidad;

	@Column(name="cli_contribuyente_especial")
	private Boolean cliContribuyenteEspecial;

	@Column(name="cli_correo")
	private String cliCorreo;

	@Column(name="cli_credito_monto")
	private BigDecimal cliCreditoMonto;

	@Column(name="cli_descuento")
	private BigDecimal cliDescuento;

	@Column(name="cli_direccion")
	private String cliDireccion;

	@Column(name="cli_dni_tipo")
	private String cliDniTipo;

	@Column(name="cli_estado")
	private String cliEstado;

	@Column(name="cli_lista_precio")
	private String cliListaPrecio;

	@Column(name="cli_nombre")
	private String cliNombre;

	@Column(name="cli_telefono")
	private String cliTelefono;

	public VenCliente() {
	}

	public String getCliDni() {
		return this.cliDni;
	}

	public void setCliDni(String cliDni) {
		this.cliDni = cliDni;
	}

	public String getCliCelular() {
		return this.cliCelular;
	}

	public void setCliCelular(String cliCelular) {
		this.cliCelular = cliCelular;
	}

	public Boolean getCliContabilidad() {
		return this.cliContabilidad;
	}

	public void setCliContabilidad(Boolean cliContabilidad) {
		this.cliContabilidad = cliContabilidad;
	}

	public Boolean getCliContribuyenteEspecial() {
		return this.cliContribuyenteEspecial;
	}

	public void setCliContribuyenteEspecial(Boolean cliContribuyenteEspecial) {
		this.cliContribuyenteEspecial = cliContribuyenteEspecial;
	}

	public String getCliCorreo() {
		return this.cliCorreo;
	}

	public void setCliCorreo(String cliCorreo) {
		this.cliCorreo = cliCorreo;
	}

	public BigDecimal getCliCreditoMonto() {
		return this.cliCreditoMonto;
	}

	public void setCliCreditoMonto(BigDecimal cliCreditoMonto) {
		this.cliCreditoMonto = cliCreditoMonto;
	}

	public BigDecimal getCliDescuento() {
		return this.cliDescuento;
	}

	public void setCliDescuento(BigDecimal cliDescuento) {
		this.cliDescuento = cliDescuento;
	}

	public String getCliDireccion() {
		return this.cliDireccion;
	}

	public void setCliDireccion(String cliDireccion) {
		this.cliDireccion = cliDireccion;
	}

	public String getCliDniTipo() {
		return this.cliDniTipo;
	}

	public void setCliDniTipo(String cliDniTipo) {
		this.cliDniTipo = cliDniTipo;
	}

	public String getCliEstado() {
		return this.cliEstado;
	}

	public void setCliEstado(String cliEstado) {
		this.cliEstado = cliEstado;
	}

	public String getCliListaPrecio() {
		return this.cliListaPrecio;
	}

	public void setCliListaPrecio(String cliListaPrecio) {
		this.cliListaPrecio = cliListaPrecio;
	}

	public String getCliNombre() {
		return this.cliNombre;
	}

	public void setCliNombre(String cliNombre) {
		this.cliNombre = cliNombre;
	}

	public String getCliTelefono() {
		return this.cliTelefono;
	}

	public void setCliTelefono(String cliTelefono) {
		this.cliTelefono = cliTelefono;
	}

}